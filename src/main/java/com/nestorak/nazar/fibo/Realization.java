package com.nestorak.nazar.fibo;

import java.util.Scanner;
/**
 * Class for contain all count methods
 * @author N.Nestorak
* */
public class Realization {

    private Scanner scanner = new Scanner(System.in);
    private int intervalBegin = scanner.nextInt();
    private int intervalEnd = scanner.nextInt();

    private void deriveOddNumber() {
        for (int i = intervalBegin; i <= intervalEnd; i++) {
            if ((i % 2) != 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    private void deriveOddNumberRevers() {
        System.out.println("Odd number from interval:");
        for (int i = intervalEnd; i >= intervalBegin; i--) {
            if ((i % 2) != 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    private void deriveSumOddNumber() {
        int sum = 0;
        for (int i = intervalBegin; i <= intervalEnd; i++) {
            if ((i % 2) != 0) {
                sum += i;
            }
        }
        System.out.println("Sum odd number in interval from " + intervalBegin
                + " to " + intervalEnd + " = " + sum);
    }

    private void derivesumEvenNumber() {
        int sum = 0;
        for (int i = intervalBegin; i <= intervalEnd; i++) {
            if ((i % 2) == 0) {
                sum += i;
            }
        }
        System.out.println("Sum even number in interval from " + intervalBegin
                + " to " + intervalEnd + " = " + sum);
    }

    public final void showRezaltOfChoose(Realization realization) {
        System.out.println("Enter interval of number: ");
        System.out.println("Make a choose from '1' to '4' what you wont to do:");
        while (true) {
            byte choose = scanner.nextByte();
            if (choose == 1) {
                realization.deriveOddNumber();
            } else if (choose == 2) {
                realization.deriveOddNumberRevers();
            } else if (choose == 3) {
                realization.deriveSumOddNumber();
            } else if (choose == 4) {
                realization.derivesumEvenNumber();
            } else{
                System.exit(0);
            }
        }
    }
}
